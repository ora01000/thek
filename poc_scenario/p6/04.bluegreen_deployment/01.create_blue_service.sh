## oc Login
oc login -u timegate -p timegate

## create project
oc new-project thek-bluegreen-demo --display-name=thek-bluegreen-demo

## create blue application
oc new-app https://gitlab.com/ora01000/bluegreen.git --name blue

## expose route
oc expose service blue --hostname=bluegreen.cloud.ocp.tg.com --name=bluegreen

