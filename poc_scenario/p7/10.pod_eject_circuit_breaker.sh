oc replace -f - <<EOF
  apiVersion: config.istio.io/v1alpha2
  kind: DestinationPolicy
  metadata:
    name: ratings-cb
  spec:
    destination:
      name: ratings
      labels:
        version: v1
    circuitBreaker:
      simpleCb:
        httpConsecutiveErrors: 1
        sleepWindow: 15m
        httpDetectionInterval: 10s
        httpMaxEjectionPercent: 100
EOF
