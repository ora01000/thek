ansible nodes -m shell -a 'systemctl stop atomic-openshift-node'
ansible masters -m shell -a 'systemctl restart atomic-openshift-master-api'
ansible masters -m shell -a 'systemctl restart atomic-openshift-master-controllers'
ansible nodes -m shell -a 'systemctl start atomic-openshift-node'

