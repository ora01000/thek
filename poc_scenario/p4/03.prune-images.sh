##
# --all
# --confirm
# --force-insecure
# --keep-tag-revisions=3
# --keep-younger-then=60m
# --prune-over-size-limit
# --prune-registry
# --registry-url

oc adm prune images <option>
