oc adm prune builds --confirm
oc adm prune builds --orphans
oc adm prune builds --keep-complete=5
oc adm prune builds --keep-failed=1
oc adm prune builds --keep-younger-then=60m
