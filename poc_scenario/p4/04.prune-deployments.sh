oc adm prune deployments --confirm
oc adm prune deployments --orphans
oc adm prune deployments --keep-complete=5
oc adm prune deployments --keep-failed=1
oc adm prune deployments --keep-younger-then=60m
