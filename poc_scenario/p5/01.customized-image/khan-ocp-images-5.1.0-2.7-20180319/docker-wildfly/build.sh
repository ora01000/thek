#/bin/bash
# -------------------------------------------------------------
#   KHAN [apm]                        http://www.opennaru.com/
#   for Docker Wildfly Image Monitoring
#
#   contact : support@opennaru.com
#   Copyright(C) 2017, Opennaru,Inc. All Rights Reserved.
# -------------------------------------------------------------

#export REGISTRY_URL=private.repository.url

#
# registry login examples
#
#docker login -u devadmin -p $(oc whoami -t) $REGISTRY_URL

#sudo docker login -p <TOKEN_IN_OPENSHIFT_REGISTRY> -e unused -u unused $REGISTRY_URL

docker build --rm --tag=khan-wildfly .

docker tag khan-wildfly jboss/wildfly-khanapm:5.1.0-1.6


#docker tag khan-wildfly $REGISTRY_URL/wildfly/wildfly-khanapm:5.1.0-1.6
#docker push $REGISTRY_URL/wildfly/wildfly-khanapm:5.1.0-1.6
