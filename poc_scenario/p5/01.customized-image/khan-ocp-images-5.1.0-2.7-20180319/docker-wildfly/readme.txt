Change KHAN [apm] server's IP and port
contrib/khan-apm.sh
KHAN_HOST
KHAN_PORT


* Change Wildfile version
- Dockerfile
FROM jboss/wildfly:11.0.0.Final
#FROM jboss/wildfly:10.1.0.Final

ENV WILDFLY_VERSION 11.0.0.Final
#ENV WILDFLY_VERSION 10.1.0.Final


* run with khan_application name

docker run -p 8080:8080 -e KHAN_APPLICATION_NAME=docker-wildfly jboss/wildfly-khanapm:5.1.0-1.5.0


docker exec -it c3c95c387b8a /bin/bash
