#!/bin/sh

# --- KHAN configuration start ---
export BUILD_ID=${HOSTNAME%-*}
export PROJ_NAME=${BUILD_ID%-*}
export INS_ID_TEMP=${HOSTNAME#$PROJ_NAME}

if [ -n "$KHAN_INSTANCE_NAME" ]; then
    export KHAN_INSTANCE_ID=${KHAN_INSTANCE_NAME}-${INS_ID_TEMP:1}
else
    export KHAN_INSTANCE_ID=${HOSTNAME:0:6}-${INS_ID_TEMP:1}
fi

if [ -z "$KHAN_INSTANCE_ID" ]; then
    export KHAN_INSTANCE_ID=${HOSTNAME%-*}
fi

if [ -z "$KHAN_APPLICATION_NAME" ]; then
    # if has OPENSHIFT_PROJECT_NAME
    if [ -z "$OPENSHIFT_PROJECT_NAME" ]; then
        export KHAN_APPLICATION_NAME=${HOSTNAME%-*}
    else
        export KHAN_APPLICATION_NAME=${OPENSHIFT_PROJECT_NAME}-${HOSTNAME%-*}
    fi
fi

if [ -z "$KHAN_HOST" ]; then
    #export KHAN_HOST=192.168.23.117
    export KHAN_HOST=192.168.23.230
fi

if [ -z "$KHAN_PORT" ]; then
    export KHAN_PORT=80
fi

if [ -z "$KHAN_APDEX_THRESHOLD" ]; then
    export KHAN_APDEX_THRESHOLD=3.0
fi

if [ -z "$KHAN_TRANSACTION_TRACE_ENABLED" ]; then
    export KHAN_TRANSACTION_TRACE_ENABLED=true
fi

if [ -z "$KHAN_TRANSACTION_TRACE_THRESHOLD" ]; then
    export KHAN_TRANSACTION_TRACE_THRESHOLD=500
fi

if [ -z "$KHAN_SQL_CAPTURE_ENABLED" ]; then
    export KHAN_SQL_CAPTURE_ENABLED=true
fi

if [ -z "$KHAN_TRANSACTION_SAMPLING_INTERVAL" ]; then
    export KHAN_TRANSACTION_SAMPLING_INTERVAL=1
fi

if [ -z "$KHAN_DB_CONN_LEAK_WARNING" ]; then
    export KHAN_DB_CONN_LEAK_WARNING=false
fi

if [ e$KHAN_AGENT_ENABLE = "efalse" ]; then
    export KHAN_AGENT_ENABLE=false
else
    export KHAN_AGENT_ENABLE=true
fi

#export JBOSS_LOGMANAGER_DIR="/opt/eap/modules/system/layers/base/.overlays/layer-base-jboss-eap-6.4.17.CP/org/jboss/logmanager/main"
#export JBOSS_LOGMANAGER_DIR="/opt/eap/modules/system/layers/base/.overlays/layer-base-jboss-eap-6.4.16.CP/org/jboss/logmanager/main"
#export JBOSS_LOGMANAGER_DIR="/opt/eap/modules/system/layers/base/.overlays/layer-base-jboss-eap-6.4.15.CP/org/jboss/logmanager/main"
#export JBOSS_LOGMANAGER_DIR="/opt/eap/modules/system/layers/base/.overlays/layer-base-jboss-eap-6.4.14.CP/org/jboss/logmanager/main"
#export JBOSS_LOGMANAGER_DIR="/opt/eap/modules/system/layers/base/.overlays/layer-base-jboss-eap-6.4.13.CP/org/jboss/logmanager/main"
#export JBOSS_LOGMANAGER_DIR="/opt/eap/modules/system/layers/base/.overlays/layer-base-jboss-eap-6.4.12.CP/org/jboss/logmanager/main"
#export JBOSS_LOGMANAGER_DIR="/opt/eap/modules/system/layers/base/.overlays/layer-base-jboss-eap-6.4.11.CP/org/jboss/logmanager/main"
#export JBOSS_LOGMANAGER_JAR=`cd "$JBOSS_LOGMANAGER_DIR" && ls -1 *.jar`

# uncomment to override memory settings
#export JAVA_OPTS_APPEND="-Xms2048m -Xmx2048m "
#export JAVA_OPTS_APPEND="$JAVA_OPTS_APPEND -Xms128m -Xmx256m "
export JAVA_OPTS_APPEND="$JAVA_OPTS_APPEND -XX:+UseParallelGC -XX:+UseParallelOldGC"

export JAVA_OPTS_APPEND="$JAVA_OPTS_APPEND -XX:+HeapDumpOnOutOfMemoryError "
export JAVA_OPTS_APPEND="$JAVA_OPTS_APPEND -XX:HeapDumpPath=/opt/eap/khan-apm "

if [ e$KHAN_AGENT_ENABLE = "etrue" ]; then
#export JAVA_OPTS_APPEND="$JAVA_OPTS_APPEND -Djava.util.logging.manager=org.jboss.logmanager.LogManager"
#export JAVA_OPTS_APPEND="$JAVA_OPTS_APPEND -Xbootclasspath/p:$JBOSS_LOGMANAGER_DIR/$JBOSS_LOGMANAGER_JAR"
export JAVA_OPTS_APPEND="$JAVA_OPTS_APPEND -noverify -javaagent:/opt/eap/khan-apm/khan-agent/khan-agent-$KHAN_AGENT_VERSION.jar"
fi

export JAVA_OPTS_APPEND="$JAVA_OPTS_APPEND -Djboss.modules.system.pkgs=org.jboss.byteman,com.opennaru.khan.agent,org.jboss.logmanager,jdk.nashorn.api"
export JAVA_OPTS_APPEND="$JAVA_OPTS_APPEND -DKHAN_INSTANCE_ID=$KHAN_INSTANCE_ID"
export JAVA_OPTS_APPEND="$JAVA_OPTS_APPEND -DKHAN_APPLICATION_NAME=$KHAN_APPLICATION_NAME"
export JAVA_OPTS_APPEND="$JAVA_OPTS_APPEND -DKHAN_HOST=$KHAN_HOST"
export JAVA_OPTS_APPEND="$JAVA_OPTS_APPEND -DKHAN_PORT=$KHAN_PORT"
export JAVA_OPTS_APPEND="$JAVA_OPTS_APPEND -DKHAN_APDEX_THRESHOLD=$KHAN_APDEX_THRESHOLD"
export JAVA_OPTS_APPEND="$JAVA_OPTS_APPEND -DKHAN_TRANSACTION_TRACE_ENABLED=$KHAN_TRANSACTION_TRACE_ENABLED"
export JAVA_OPTS_APPEND="$JAVA_OPTS_APPEND -DKHAN_TRANSACTION_TRACE_THRESHOLD=$KHAN_TRANSACTION_TRACE_THRESHOLD"
export JAVA_OPTS_APPEND="$JAVA_OPTS_APPEND -DKHAN_SQL_CAPTURE_ENABLED=$KHAN_SQL_CAPTURE_ENABLED"
export JAVA_OPTS_APPEND="$JAVA_OPTS_APPEND -DKHAN_TRANSACTION_SAMPLING_INTERVAL=$KHAN_TRANSACTION_SAMPLING_INTERVAL"
export JAVA_OPTS_APPEND="$JAVA_OPTS_APPEND -DKHAN_DB_CONN_LEAK_WARNING=$KHAN_DB_CONN_LEAK_WARNING"

# --- KHAN configuration end ---