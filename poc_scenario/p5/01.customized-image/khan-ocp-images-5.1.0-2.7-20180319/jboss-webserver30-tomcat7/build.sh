#/bin/bash
# -------------------------------------------------------------
#   KHAN [apm]                        http://www.opennaru.com/
#   for Openshift Container JBoss Web Tomcat 7 
#
#   contact : service@opennaru.com
#   Copyright(C) 2015, Opennaru,Inc. All Rights Reserved.
# -------------------------------------------------------------

#export REGISTRY_URL=172.30.22.235:5000
export REGISTRY_URL=docker-registry-default.app.ocp-dev.opennaru.com

#
# registry login examples
#

#oc login
#docker login -u devadmin -p $(oc whoami -t) $REGISTRY_URL

#sudo docker login -p <TOKEN_IN_OPENSHIFT_REGISTRY> -e unused -u unused $REGISTRY_URL
#oc login --token <TOKEN_IN_OPENSHIFT_REGISTRY> ocp-master1.ocp-dev.opennaru.com

docker build --rm --tag=khan-jbossweb-tomcat7 .

docker tag khan-jbossweb-tomcat7 $REGISTRY_URL/openshift/jbossweb-tomcat7-khanapm:5.1.0-1.6
docker push $REGISTRY_URL/openshift/jbossweb-tomcat7-khanapm:5.1.0-1.6
