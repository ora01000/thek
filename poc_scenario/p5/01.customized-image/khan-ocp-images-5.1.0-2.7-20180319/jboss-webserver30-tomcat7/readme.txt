Change KHAN [apm] server's IP and port
contrib/khan-apm.sh
KHAN_HOST
KHAN_PORT

Change OCP base images tag version in Dockerfile
ENV BASE_IMG_VERSION 1.3
#ENV BASE_IMG_VERSION 1.2
